﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LOD_AI_PROJECT.Pathfinding;
using LOD_AI_PROJECT.Worlds;

namespace LOD_AI_PROJECT.Agents
{
    public class Agent : Updateable
    {

        public Agent(World parentWorld, Location spawnLocation)
        {
            this.parentWorld = parentWorld;
            parentWorld.RegisterForMainUpdate(this);
            CurrentLocation = spawnLocation;
        }

        public World parentWorld { get; private set; }

        public Location CurrentLocation { get; protected set; }
     
        public virtual void Update(int deltaTime)
        {

        }
    }
}
