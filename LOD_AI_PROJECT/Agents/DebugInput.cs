﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LOD_AI_PROJECT.Worlds;

namespace LOD_AI_PROJECT.Agents
{
    class DebugInput : Updateable
    {
        bool monitoringInput = false;
        World parentWorld;

        public Thread updateDetectionThread;
        public List<ConsoleKey> lastKeysPressed = new List<ConsoleKey>();
        List<Input> inputList = new List<Input>();

        public bool activated = true;

        public DebugInput(bool isStepping, World parentWorld)
        {
            monitoringInput = isStepping;
            this.parentWorld = parentWorld;
            this.parentWorld.RegisterForMainUpdate(this);

            inputList.Add(new Input(ConsoleKey.Escape, KillWorld));
            inputList.Add(new Input(ConsoleKey.Spacebar, SayDeltaTime));

            updateDetectionThread = new Thread(new ThreadStart(CheckForInput));
            updateDetectionThread.Name = "Input Thread";
            updateDetectionThread.Start();
        }

        public void CheckForInput()
        {
            while(activated)
            {
                ConsoleKey keyPressed = Console.ReadKey().Key;
                if(!activated)
                {
                    return;
                }
                lock(lastKeysPressed)
                {
                    lastKeysPressed.Add(keyPressed);
                }
            }
        }

        void KillWorld(int deltaTime)
        {
            lastKeysPressed.Clear();
            activated = false;
            updateDetectionThread.Join();
            Console.WriteLine("Killed input thread");
            parentWorld.EndWorld();
        }

        void SayDeltaTime(int deltaTime)
        {
            Console.WriteLine("Delta time is: " + deltaTime);
        }

        public void Update(int deltaTime)
        {
            if(monitoringInput)
            {
                lock(lastKeysPressed)
                {
                    for(int i = 0; i < inputList.Count; i++)
                    {
                        inputList[i].Check(lastKeysPressed,deltaTime);
                    }
                    lastKeysPressed.Clear();
                }
            }
        }

        class Input
        {
            System.Action<int> inputFunction;
            ConsoleKey listeningKey;

            public Input(ConsoleKey listeningKey, System.Action<int> inputFunction)
            {
                this.listeningKey = listeningKey;
                this.inputFunction = inputFunction;
            }

            public bool Check(List<ConsoleKey> comparingList, int deltaTime)
            {
                if(comparingList.Contains(listeningKey))
                {
                    inputFunction(deltaTime);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
