﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LOD_AI_PROJECT;
using LOD_AI_PROJECT.Worlds;
using LOD_AI_PROJECT.Pathfinding;

namespace LOD_AI_PROJECT.Agents
{
    class WalkingAgent : Agent
    {
        Location targetLocation;
        bool hasValidPath = false;
        bool atTarget = false;
        bool lost = false;
        Path[] pathToTargetLocation;
        int positionInPath = 0;

        public WalkingAgent(World parentWorld, Location spawnLocation, Location initalTarget) : base(parentWorld,spawnLocation)
        {
            targetLocation = initalTarget;
            positionInPath = 0;
        }

        public override void Update(int deltaTime)
        {
            if(lost)
            {
                return;
            }
            if (atTarget)
            {

            }
            else
            {
                if (hasValidPath)
                {
                    if(CurrentLocation.ConnectingPaths.Contains(pathToTargetLocation[positionInPath]))
                    {
                        CurrentLocation = pathToTargetLocation[positionInPath].EndLocation;
                    }
                }
                else
                {
                    CrudeLocationPathfinder pathfinder = new CrudeLocationPathfinder(CurrentLocation, targetLocation);
                    if (pathfinder.Search(out pathToTargetLocation))
                    {
                        hasValidPath = true;

                        positionInPath = 0;
                    }
                    else
                    {
                        lost = true;
                    }
                }
            }
        }
    }
}
