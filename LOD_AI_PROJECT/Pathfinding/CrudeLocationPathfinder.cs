﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Pathfinding
{
    class CrudeLocationPathfinder : Pathfinder
    {
        List<Path> route;
        List<Location> visitedLocations;
        Location initialLocation;
        Location endingPoint;

        public CrudeLocationPathfinder(Location startingPoint, Location finishPoint)
        {
            initialLocation = startingPoint;
            visitedLocations = new List<Location>();
            route = new List<Path>();
            endingPoint = finishPoint;
        }

        public bool Search(out Path[] route)
        {
            if(CheckLocation(initialLocation))
            {
                route = this.route.ToArray();
                return true;
            }
            else
            {
                route = null;
                return false;
            }
        }

        bool CheckLocation(Location searchLocation)
        {
            for(int i = 0; i < searchLocation.ConnectingPaths.Count; i++)
            {
                if(visitedLocations.Contains(searchLocation.ConnectingPaths[i].EndLocation))
                {
                    continue;
                }
                if(searchLocation.ConnectingPaths[i].EndLocation == endingPoint)
                {
                    route.Add(searchLocation.ConnectingPaths[i]);
                    return true;
                }
                if(searchLocation.ConnectingPaths[i].EndLocation.ConnectingPaths.Count != 0)
                {
                    route.Add(searchLocation.ConnectingPaths[i]);
                    if(CheckLocation(searchLocation.ConnectingPaths[i].EndLocation))
                    {
                        return true;
                    }
                    else
                    {
                        route.RemoveAt(route.Count - 1);
                        continue;
                    }
                }
            }
            return false;
        }


    }
}
