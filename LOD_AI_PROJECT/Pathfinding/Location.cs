﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Pathfinding
{
    public class Location
    {
        public string Name { get; private set; }

        public List<Path> ConnectingPaths { get; private set; }

        public List<Path> GoingPaths { get; private set; }

        public Worlds.World parentWorld;

        public Location(Worlds.World parentWorld)
        {
            ConnectingPaths = new List<Path>();
            this.parentWorld = parentWorld;
            parentWorld.AddLocation(this);
        }
    }
}
