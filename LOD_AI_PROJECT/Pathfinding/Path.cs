﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Pathfinding
{
    public class Path : Location
    {
        public float Cost { get; private set; }

        public Location StartLocation { get; private set; }

        public Location EndLocation { get; private set; }

        public static Path TryCreatePath(Location startLocation, Location endLocation)
        {
            if(startLocation != endLocation && startLocation.parentWorld == endLocation.parentWorld)
            {
                return new Path(startLocation,endLocation,startLocation.parentWorld);
            }
            else
            {
                return null;
            }
        }

        private Path(Location startLocation, Location endLocation,Worlds.World parentWorld) : base(parentWorld)
        {
            StartLocation = startLocation;
            StartLocation.ConnectingPaths.Add(this);

            EndLocation = endLocation;
        }
    }
}
