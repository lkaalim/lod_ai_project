﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Pathfinding
{
    struct Vector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3 Normalized
        {
            get
            {
                return this * Magnitude;
            }
        }

        public static Vector3 Zero()
        {
            return new Vector3(0, 0, 0);
        }

        public float Magnitude
        {
            get
            {
                return (Math.Sqrt(x * x) + (x * x) + (x * x));
            }
        }

        public static Vector3 operator *(Vector3 v1, Vector3 v2)
        {
            return Vector3(v1.x*v2.x, v1.y * v2.y, v1.z* v2.z);
        }

        public static Vector3 operator *(Vector3 v1, float scalar)
        {
            return Vector3(v1.x * scalar, v1.y * scalar, v1.z * scalar);
        }
    }
}
