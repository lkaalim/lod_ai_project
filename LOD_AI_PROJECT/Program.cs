﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Begin AI Simulation");
            
            Worlds.World activeWorld = Worlds.World.BasicWorld();
            
            Agents.DebugInput debug = new Agents.DebugInput(true,activeWorld);
            Agents.WalkingAgent basicAgent = new Agents.WalkingAgent(activeWorld,activeWorld.Locations[0],activeWorld.Locations[1]);

            activeWorld.updateThread.Join();


            Console.WriteLine("Simulation Terminated");
            Console.ReadKey();
            return;
        }
    }
}
