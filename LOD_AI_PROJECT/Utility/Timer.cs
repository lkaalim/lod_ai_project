﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Utility
{
    class Timer
    {
        float timeRemaining;
        float startingTime;

        Action onFinishCallback;

        public float Progress { get { return timeRemaining / startingTime; } }

        public Timer(float startingTime)
        {
            this.startingTime = startingTime;
            timeRemaining = startingTime;
        }

        public Timer(float startingTime, Action onFinish)
        {
            this.startingTime = startingTime;
            timeRemaining = startingTime;
            onFinishCallback = onFinish;
        }

        public bool Update(int deltaTime)
        {
            timeRemaining -= ((float)deltaTime / 1000);
            if (timeRemaining <= 0)
            {
                if(onFinishCallback != null)
                {
                    onFinishCallback();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
