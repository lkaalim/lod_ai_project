﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOD_AI_PROJECT.Worlds
{
    public interface Updateable
    {
        // Delta time: time in miliseconds since last update
        void Update(int deltaTime);

    }
}
