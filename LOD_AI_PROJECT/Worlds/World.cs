﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using LOD_AI_PROJECT.Pathfinding;

namespace LOD_AI_PROJECT.Worlds
{
    public class World
    {

        public int TimeBetweenUpdates { get; private set; }

        public bool Activated { get; private set; }

        public List<Location> Locations { get; private set; }

        public bool Sleeping { get; private set; }

        public Thread updateThread;

        int updateCount = 0;

        public static World BasicWorld()
        {
            World basicWorld = StartNewWorld();
            
            Location startingLocation = new Location(basicWorld);

            Location endingLocation = new Location(basicWorld);

            Path mainPath = Path.TryCreatePath(startingLocation, endingLocation);

            basicWorld.Start();
            basicWorld.TimeBetweenUpdates = 0;

            return basicWorld;
        }

        public void EndWorld()
        {
            Activated = false;
        }

        public void AddLocation(Location newLocation)
        {
            Locations.Add(newLocation);
        }

        public void Start()
        {
            updateThread.Start();
        }

        public static World StartNewWorld()
        {
            World newWorld = new World("World?");

            newWorld.updateThread = new Thread(new ThreadStart(newWorld.Initalize));
            newWorld.updateThread.Name = "World Thread";

            return newWorld;
        }

        public void Initalize()
        {
            Activated = true;
            Sleeping = false;
            
            while (Activated)
            {
                UpdateAllEntities();
                Sleeping = true;
                Thread.Sleep(TimeBetweenUpdates);
                Sleeping = false;
            }

            Console.WriteLine("World Died");
        }


        private World(string name)
        {
            Locations = new List<Location>();
            EntitiesToUpdate = new List<Updateable>();
        }



        DateTime timeWhenLastUpdated;

        void UpdateAllEntities()
        {
            int deltaTime = DateTime.Now.Subtract(timeWhenLastUpdated).Milliseconds;
            timeWhenLastUpdated = DateTime.Now;

            //Replace with a neater Console Update Function
            Console.Clear();
            updateCount++;
            Console.WriteLine("Update Count: " + updateCount);


            for (int i = 0; i < EntitiesToUpdate.Count; i++)
            {
                EntitiesToUpdate[i].Update(deltaTime);
            }
        }

        public void RegisterForMainUpdate(Updateable updatable)
        {
            lock(EntitiesToUpdate)
            {
                EntitiesToUpdate.Add(updatable);
            }
        }

        public void UnRegisterForMainUpdate(Updateable updatable)
        {
            lock (EntitiesToUpdate)
            {
                EntitiesToUpdate.Remove(updatable);//TODO: optimize
            }
        }


        public List<Updateable> EntitiesToUpdate;
    }
}
